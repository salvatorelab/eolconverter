package converters;

public enum EolConversion {
    CR,
    LF,
    CRLF
}