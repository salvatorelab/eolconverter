package converters.encoding;

public enum Encoding {
    UTF8,
    UTF16BE,
    UTF16LE,
    UTF32BE,
    UTF32LE,
    UNKNOWN
}
