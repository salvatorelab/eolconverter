# End of line converter
This small tool can convert line terminators from and to the following formats:
 - Mac (CR)
 - Win (CRLF)
 - Unix (LF)


It supports and automatically detects the following file encodings:  
 - UTF8
 - UTF8 without BOM
 - UTF16
 - UTF16 without BOM
 - UTF32
 - UTF32 without BOM

For UTF16 and UTF32 it also takes into account endianness.  

Please note: Although this tool has been tested with different file formats and edge cases it is not recommended to use it in production. I have invested a limited time into it and the main purpose was learning about the internals of file encoding. But feel free to try it out and report any issue or contribute if you find it useful!
